# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require "faker"

5.times.each do
    puts Faker::Internet.email(domain: 'gmail')
    User.create!(name: Faker::Name.name , email: Faker::Internet.email(domain: 'gmail') ,
     nickname: Faker::Name.first_name , birthdate: "01/01/1990", gender: rand(0..1) , contact_phone: "12345678910")
end
10.times.each do
    Post.create!(content_text: Faker::Lorem.paragraph_by_chars(number: 256, supplemental: false), user_id:rand(1..5))
end