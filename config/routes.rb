Rails.application.routes.draw do
  post 'auth/login'
  post 'auth/signup'
  
  get 'comment/:id', to: "comment#show"
  put'comment/:id', to: "comment#update"
  delete 'comment/:id', to: "comment#destroy"
  post 'comment', to: "comment#create"
  get 'comment', to: "comment#index"
  
  post 'posts/add_like', to: "posts#create_like"
  get 'posts/:id', to: "posts#show"
  put 'posts/:id', to: "posts#update"
  delete 'posts/:id', to: "posts#destroy"
  post 'posts', to: "posts#create"
  get 'posts', to: "posts#index"
 
  put 'users/add_photo/:id', to: "users#add_photo"
  post 'users', to: "users#create"
  get 'users', to: "users#index"
  get 'users/:id', to: "users#show"
  put'users/:id', to: "users#update"
  delete 'users/:id', to: "users#destroy"
 
 
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
