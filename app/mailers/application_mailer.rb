class ApplicationMailer < ActionMailer::Base
  default from: 'pedropalermo97@gmail.com'
  layout 'mailer'
end
