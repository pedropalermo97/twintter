class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :nickname, :gender, :email,
   :birthdate, :contact_phone ,:image, :followers_length,
   :followeds_length, :followeds_name, :followers_name  
  has_many :posts 


  
  def followers_length
   # obj = []
   # object.followers.each do |t|
   #   obj.push(t.name)
  #end
    return object.followers.length
  end

  def followeds_length
    # obj = []
    # object.followers.each do |t|
    #   obj.push(t.name)
   #end
     return object.followeds.length
   end
  
   def followers_name
       obj = []
       object.followers.each do |t|
      obj.push(t.name)
      end
      return obj
   end

   def followeds_name
     obj = []
     object.followeds.each do |t|
       obj.push(t.name)
    end
    return obj
   end

end
