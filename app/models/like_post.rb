class LikePost < ApplicationRecord
  #Relações
  belongs_to :user
  belongs_to :post
  #validações
  validate :already_liked?
  #funções
  def already_liked?
    if post.users.include?(user)
      errors.add(:already_liked, "Você já deu like nesse post")
    end
  end
  
end