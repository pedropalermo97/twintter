class LikeComment < ApplicationRecord
  #Relações
  belongs_to :comment
  belongs_to :user

  #validações
  validate :already_liked?
  #funções
  def already_liked?
    if comment.users.include?(user)
      errors.add(:already_liked, "Você já deu like nesse comentário")
    end
  end
  
end