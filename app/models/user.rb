class User < ApplicationRecord
    has_secure_password
    mount_uploader :image, ImageUploader

    #Relações
    has_many :posts , dependent: :destroy
    has_many :like_posts
    has_many :like_comments
    has_many :comments
    has_many :follows
    #has_many :liked_posts , class_name:"Post" , through: :like_posts 
    #has_many :follows
    has_and_belongs_to_many :followers, join_table: "follows", foreign_key: "followed_id", class_name: "User", association_foreign_key: "follower_id"
    has_and_belongs_to_many :followeds, join_table: "follows", foreign_key: "follower_id", class_name: "User", association_foreign_key: "followed_id"
   
    #validações
    validates :name, :email , :birthdate, :gender, :contact_phone, :nickname, presence: true
    validates :email, uniqueness: true
    validates :contact_phone, length: {is:11}, numericality: {only_integer: true}
    validates :password, :password_confirmation, length: {minimum: 6}
    VALIDATE_EMAIL =  /[^#!@$%&]+@+[a-z]+\.[a-z]/
    validates :email, format: {with: VALIDATE_EMAIL, message: "Utilize o seu email da UFF. Exemplo: meuemail@gmail.com"}
    validate :under_age?
    enum gender:{
        "Women":0,
        "Men":1
    }
    before_create :generate_validation_token
    #funções
    def age
        ((Date.today-birthdate).to_i/365.25).to_i
    end
    
    def under_age?
        if birthdate.nil? || age < 18
            errors.add(:under_age,"You are under age")
        end
    end

    def destroy_follows
        followeds=Follow.whe(follower_id:id)
        followeds.each do |t|
            t.destroy
        end
        followers=Follow.whe(follower_id:id)
        followers.each do |t|
            t.destroy
        end
    end

    def generate_validation_token
       validate_token = generate_random_token
       validate_token_expiry_at = Time.now + 5.minutes 
    end

    def validate_user?(token)
        if validate_token_expiry_at > Time.now
            self.is_valid = true
            self.validate_token=nil
            return true if self.save
        else
            return false
        end
    end

    private
        def generate_random_token
            SecureRandom.alphanumeric(15)
        end
end

